{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}

------------------------------------------------------------------------
-- |
-- Module      :  Control.Concurrent.Async.Class
-- Copyright   :  (C) 2013 Toralf Wittner
-- License     :  MPL-2.0 (see the file LICENSE)
--
-- Maintainer  :  Toralf Wittner <tw@dtex.org>
-- Stability   :  provisional
-- Portability :  non-portable (requires concurrency)
--
------------------------------------------------------------------------
module Control.Concurrent.Async.Class (Async (..)) where

import Control.Monad.Trans.Class
import Control.Monad.Trans.Either
import Control.Monad.Trans.Reader

import qualified Control.Concurrent.Async         as A
import qualified Control.Monad.Trans.State.Lazy   as L
import qualified Control.Monad.Trans.State.Strict as S

class (Monad m, Monad n) => Async m n where
    async :: m a -> n (A.Async a)

instance Async IO IO where
    async = A.async
    {-# INLINE async #-}

instance Async m n => Async (ReaderT r m) (ReaderT r n) where
    async = mapReaderT async
    {-# INLINE async #-}

instance Async m n => Async (S.StateT s m) (S.StateT s n) where
    async m = S.StateT $ \s -> do
        a <- async (S.evalStateT m s)
        return (a, s)
    {-# INLINE async #-}

instance Async m n => Async (L.StateT s m) (L.StateT s n) where
    async m = L.StateT $ \s -> do
        a <- async (L.evalStateT m s)
        return (a, s)
    {-# INLINE async #-}

instance Async m n => Async m (EitherT e n) where
    async = lift . async
    {-# INLINE async #-}
